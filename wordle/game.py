import uuid
import random

from flask import Blueprint
from flask import request, session, abort, jsonify
from flask_cors import CORS

from flasgger import swag_from

from wordle import strings

bp = Blueprint('game', __name__, url_prefix='/game')
CORS(bp, origins=["http://localhost:5173", "http://127.0.0.1:5173"], supports_credentials=True)

GREEN = 'G'
YELLOW = 'Y'
RED = 'R'

MASTERMIND = 'mastermind'
WORDLE = 'wordle'

words = list()

def _legal_words():
    with open('words.txt', 'r') as f:
        lines = f.readlines()
    return [word.strip() for word in lines]

def _load_words(userid):
    with open('words.txt', 'r') as f:
        lines = f.readlines()
    if session['mode'][userid].lower() == MASTERMIND:
        return [word.strip() for word in lines if len(word) == len(set(word))]
    else:
        return [word.strip() for word in lines]    

def _load_session():
    for item in ['names', 'answers', 'guesses', 'feedbacks', 'mode']:
        if item not in session:
            session[item] = dict()

def _check_session():
    if 'session' not in request.cookies:
        abort(400, strings.SESSION_ERROR)

@bp.errorhandler(400)
def something_went_wrong(e):
    return jsonify(error=str(e)), 400

@bp.route("/register", methods=['POST'])
@swag_from("specs/register.yml")
def register():
    """
    Registration route. POST your name to this to create a new id.
    ids are stored in a cookie-based-session-based dictionary. 
    """
    _load_session()
    if request.method == 'POST':
        #if request.mimetype != "application/json":
        #    raise ValidueError()
        req = request.get_json(silent=False)
        if "name" not in req:
            return {"id": None, 'message': strings.NEEDS_NAME}, 400
        name = req["name"]
        generated_id = str(uuid.uuid1())
        session['names'][generated_id] = name
        mode = req.get('mode', WORDLE)
        session['mode'][generated_id] = mode
        session.modified = True
        return {"id": generated_id, "message": strings.CREATED}, 201

@bp.route("/create", methods=['POST'])
@swag_from("specs/create.yml")
def create():
    """
    Game creation route. POST your id to this to create a new game.
    This associates a secret word with your ID you can guess
    with the /guess endpoint.        
    """
    _check_session()
    if request.method == 'POST':
        req = request.get_json()
        userid = req["id"]
        if userid not in session['names']:
            return {"created": False, "message": strings.REGISTER_FIRST}, 422
        should_overwrite = req.get("overwrite", False)
        has_existing_game = userid in session['answers'] 
        if should_overwrite or not has_existing_game:
            session['answers'][userid] = random.choice(_load_words(userid))
            if userid in session['guesses']:
                del session['guesses'][userid]
            if userid in session['feedbacks']:
                del session['feedbacks'][userid]
            session.modified = True
            return {"created": True, "message": strings.CREATED_GAME}, 201
        else:
            return {"created": False, "message": strings.GAME_ALREADY_EXISTS}, 200
    
@bp.route("/guess", methods=['POST'])
@swag_from("specs/guess.yml")
def guess():
    """
    Gameplay route. POST your id and your guess to try to guess the word.
    This returns feedback for your guess if you have guesses left.        
    """
    _check_session()
    if request.method == 'POST':
        req = request.get_json()
        userid = req["id"]
        guess = req["guess"]
        
        if userid not in session['names']:
            return {"feedback": None, "message": strings.REGISTER_FIRST}, 422
        
        has_existing_game = userid in session['answers'] 
        if not has_existing_game:
            return {"feedback": None, "message": strings.GAME_NOT_EXISTS}, 422
        answer = session['answers'][userid]
        
        if userid not in session['guesses']:
            session['guesses'][userid] = []
        if userid not in session['feedbacks']:
            session['feedbacks'][userid] = []

        if session['mode'][userid].lower() == MASTERMIND:
            return guess_mastermind(userid, guess, answer)
        
        session['guesses'][userid].append(guess)
        session['feedbacks'][userid].append(_judge(guess, answer))
        session.modified = True

        feedbacks = session['feedbacks'][userid]
        guesses_left = 6 - len(feedbacks)
        if feedbacks[-1] == GREEN * 5:
            return {"feedback": feedbacks[-1], "message": strings.GAME_WON + f"in {len(feedbacks)} tries", "answer": answer}, 200
        elif guesses_left <= 0:
            del session['answers'][userid]
            del session['guesses'][userid]
            del session['feedbacks'][userid]
            session.modified = True
            return {"feedback": feedbacks[-1], "message": strings.EXCEEDED_GUESSES, "answer": answer}, 422
        return {"feedback": feedbacks[-1], "message": f"{guesses_left} guesses left"}, 200

def guess_mastermind(userid: str, guess: str, answer: str):
    session['guesses'][userid].append(guess)
    session['feedbacks'][userid].append(_judge_mastermind(guess, answer))
    session.modified = True
    feedbacks = session['feedbacks'][userid]
    if guess.lower() == answer.lower():
            return {"feedback": feedbacks[-1], "message": strings.GAME_WON + f"in {len(feedbacks)} tries"}, 200
    guesses = len(feedbacks)
    return {"feedback": feedbacks[-1], "message": f"{guesses} guesses made so far"}, 200
    
def _judge_mastermind(guess: str, answer: str):
    if guess not in _legal_words():
        return -1
    print(guess, answer)
    guess = ''.join(set(guess))
    return len([ch for ch in guess if ch in answer])

def _judge(guess: str, answer: str):
    print(guess, answer)
    feedback = ""
    guess, answer = guess.lower(), answer.lower()
    for g_ch, a_ch in zip(guess, answer):
        if g_ch not in answer:
            feedback += RED
        elif g_ch == a_ch:
            feedback += GREEN
        else:
            feedback += YELLOW
    return feedback
