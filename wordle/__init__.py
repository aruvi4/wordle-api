import os, json

from flask import Flask, request
from werkzeug.middleware.proxy_fix import ProxyFix

from flasgger import Swagger, LazyString, LazyJSONEncoder

def create_app(test_config = None):
    app = Flask(__name__, instance_relative_config = True)
    #app.json_provider_class = LazyJSONEncoder
    #TODO: find out why the above line doesn't work but the below line does.
    app.json = LazyJSONEncoder(app)
    app.config.from_mapping(SECRET_KEY="shhh")
    
    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.update(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)


    from . import game
    app.register_blueprint(game.bp)

    
    app.config['SWAGGER'] = {
        'title': 'Wordle',
        'uiversion': 3,
        'openapi' : '3.0.2'
    }
    app.config.update(
        SESSION_COOKIE_SAMESITE = "None", 
        SESSION_COOKIE_SECURE = True, 
        SESSION_COOKIE_HTTPONLY = True,
        SESSION_COOKIE_PARTITIONED = True
    )
    swagger_template = {
        "swagger": "3.0",
        "info": {
            "title": "Wordle API",
            "description": "An API to create, and play Wordle games. Register yourself with /game/register, \
                and choose a game mode. Everything is cookie-based-session-based, so make sure to pass on \
                the cookie you get back from your register request for subsequent requests. Conversely\
                deleting the cookie will reset everything.",
            "license" : {
                "name": " Wordle API by Aruvi is licensed under CC BY-SA 4.0 ",
                "url": "https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1"
            },
            "version": "1.0.0"
        },
        "basePath": "wordle"
    }
    swagger_config = Swagger.DEFAULT_CONFIG
    swagger_config['swagger_ui_bundle_js'] = '//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js'
    swagger_config['swagger_ui_standalone_preset_js'] = '//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js'
    swagger_config['jquery_js'] = '//unpkg.com/jquery@2.2.4/dist/jquery.min.js'
    swagger_config['swagger_ui_css'] = '//unpkg.com/swagger-ui-dist@3/swagger-ui.css'
    swagger_template['swaggerUiPrefix'] = LazyString(lambda: request.environ.get('SCRIPT_NAME', ''))
    swagger = Swagger(app, template=swagger_template, config=swagger_config)
    return app