# Wordle API

## Installation instructions
Install all requirements using `pip3 install -r requirements.txt`.

## Running the server
`flask --app wordle run --debug`

## Running tests
`python3 -m pytest`

## Deployment
Assuming that you have access to a linux box with nginx configured to a domain name you've bought, at least rudimentarily set up to, say, serve static files off a user's home folder, you need to perform the following steps:

1. Take a build of the module (make sure that pyproject.toml is set up) and perform `python3 -m build --wheel`
2. `scp` the generated `dist/wordle-VERSION-py2.py3-none-any.whl` to the box, say, at `/home/$USER/wordle-api/`
3. `ssh` to the box and `cd /home/$USER/wordle-api/`

    1. If not already present, install `pip3` using `sudo apt install pip3` or equivalent.
    2. If not already present, install `venv` using `pip3 install venv`
    3. If not already present, create a virutal environemnt for installing and running our built application using `python3 -m venv .venv`
    4. Activate your `venv` using `source .venv/bin/activate`

4. `pip3 install <the wheel file you just scp'd>`. 
5. We will heed the ample warning given by the `flask --app wordle run --debug` development server. If not already present, install `gunicorn` to create production server workers using `pip3 install gunicorn`.
6. Test whether everything runs okay by doing `gunicorn -w 1 -b 127.0.0.1:5000 wordle:create_app()`. This should successfully spawn a gunicorn worker listening to `localhost:5000`. Do a `wget localhost:5000/apidocs` to make sure that the server started alright.
7. `Ctrl+C` this process. To avoid having to manually start this server, you'd need to make a daemon that starts this process on its own after the network loads. One simple way to do this that works on most linux boxes is using `systemd`. Do a `nano /etc/systemd/system/wordle.service` and put the following inside:

```
[Unit]
Description=wordle
After=network.target

[Service]
User=aruvi
WorkingDirectory=/home/aruvi/wordle-api
ExecStart=/home/aruvi/wordle-api/.venv/bin/gunicorn -w 1 -b 127.0.0.1:5000 --access-logfile - wordle:cr>
Restart=always

[Install]
WantedBy=multi-user.target

```
Replace my username with your own $USER, of course.
8. Now tell `systemctl` to do a `sudo systemctl daemon reload` and then start our service with `sudo systemctl start wordle`
9. The next step is to configure `nginx` to forward requests to our app via the reverse proxy mechanism. Check `etc/nginx/sites-available` and there should be a `.conf` file configured already to perform the job of serving the static html pages of users.
10. Inside it you should see this 
```
server {
    server_name [configured domain name]
    [configuration for the ~username static htmls]
    [we need to paste our app's configuration here]
}
```
11. In the specified spot, paste in
```
location / {
                proxy_pass http://127.0.0.1:5000/;
                add_header 'Access-Control-Allow-Origin' "*" always;
                add_header 'Access-Control-Allow-Headers' "Content-Type, X-CSRFTOKEN";
                add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';

                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-NginX-Proxy true;
                proxy_ssl_session_reuse off;
                proxy_set_header Host $http_host;

                proxy_read_timeout 10;
                proxy_connect_timeout 10;
                proxy_send_timeout 10;

                proxy_redirect off;
        }
```
12. Confirm that the nginx configuration file is correctly specified by running `sudo nginx -t`
13. Restart nginx using `sudo systemctl restart nginx`.
14. Now if you hit `[configured domain name]/apidocs` you should see the page being served.