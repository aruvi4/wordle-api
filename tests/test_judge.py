from wordle.game import _judge

def test_judge_allgreens():
    answer = 'SPEAR'
    guess = 'SPEAR'
    expected_feedback = 'GGGGG'
    assert(_judge(guess, answer) == expected_feedback)

def test_judge_allreds():
    answer = 'SPEAR'
    guess = 'BINGO'
    expected_feedback = 'RRRRR'
    assert(_judge(guess, answer) == expected_feedback)

def test_judge_allyellows():
    answer = 'SPEAR'
    guess = 'REAPS'
    expected_feedback = 'YYYYY'
    assert(_judge(guess, answer) == expected_feedback)

def test_judge_generic():
    answer = 'SPEAR'
    guess = 'OPINE'
    expected_feedback = 'RGRRY'
    assert(_judge(guess, answer) == expected_feedback)