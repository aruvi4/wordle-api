import pytest, uuid
from flask.testing import FlaskClient

def setup_session(client: FlaskClient):
    with client.session_transaction() as session:
        for dictname in ['names', 'answers', 'guesses', 'feedbacks', 'mode']:
            session[dictname] = dict()
        return session

def test_register_success(client: FlaskClient):
    url = "/game/register"
    mimetype = "application/json"
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }
    data = {"name": "Aruvi"}
    response = client.post(url, json=data, headers=headers)
    assert(response.status_code == 201)
    json_resp = response.get_json()
    assert("id" in json_resp)
    assert(uuid.UUID(json_resp["id"]))

def test_create_unregistered(client: FlaskClient):
    url = "/game/create"
    mimetype = "application/json"
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }
    test_id = uuid.uuid1()
    data = {"id": str(test_id)}
    setup_session(client)
    response = client.post(url, json=data, headers=headers)

    assert(response.status_code == 422)
    assert(not response.get_json()["created"])

def test_create_already_exists(client: FlaskClient):
    url = "/game/create"
    mimetype = "application/json"
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }
    test_id = str(uuid.uuid1())
    data = {"id": test_id}
    setup_session(client)
    with client.session_transaction() as session:
        session['names'] = {test_id: "Non Null Name"}
        session['answers'] = {test_id: "spear"}
    response = client.post(url, json=data, headers=headers)
    assert(response.status_code == 200)
    assert(not response.get_json()["created"])

def test_create_new(client: FlaskClient):
    url = "/game/create"
    mimetype = "application/json"
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }
    test_id = str(uuid.uuid1())
    data = {"id": test_id}
    setup_session(client)
    with client.session_transaction() as session:
        session['names'] = {test_id: "Non Null Name"}
        session['answers'] = dict()
    response = client.post(url, json=data, headers=headers)
    assert(response.status_code == 201)
    assert(response.get_json()["created"])

def test_create_overwrite(client: FlaskClient):
    url = "/game/create"
    mimetype = "application/json"
    headers = {
        'Content-Type': mimetype,
        'Accept': mimetype
    }
    test_id = str(uuid.uuid1())
    data = {"id": test_id, "overwrite": True}
    setup_session(client)
    with client.session_transaction() as session:
        session['names'] = {test_id: "Non Null Name"}
        session['answers'] = {test_id: 'spear'}
    response = client.post(url, json=data, headers=headers)
    assert(response.status_code == 201)
    assert(response.get_json()["created"])
    with client.session_transaction() as session:
        assert(session['answers'] != 'spear')

def test_guess_perfect_first(client: FlaskClient):
    '''
    A perfect guess in the first attempt
    '''
    url = "/game/guess"
    mimetype = "application/json"
    headers = {
        "Content-Type": mimetype,
        "Accept": mimetype
    }
    test_id = str(uuid.uuid1())
    data = {"id": test_id, "guess": "SPEAR"}
    setup_session(client)
    with client.session_transaction() as session:
        session['names'] = {test_id: "Non Null Name"}
        session['answers'] = {test_id: "spear"}
        session['mode'] = {test_id: "wordle"}
    response = client.post(url, json=data, headers=headers)
    assert(response.status_code == 200)
    assert(response.get_json()["feedback"] == "GGGGG")

def test_guess_seventh(client: FlaskClient):
    '''
    Trying to guess for the seventh time
    '''
    url = "/game/guess"
    mimetype = "application/json"
    headers = {
        "Content-Type": mimetype,
        "Accept": mimetype
    }
    test_id = str(uuid.uuid1())
    data = {"id": test_id, "guess": "MANGO"}
    setup_session(client)
    with client.session_transaction() as session:
        session['names'] = {test_id: "Non Null Name"}
        session['mode'] = {test_id: "wordle"}
        session['answers'] = {test_id: "spear"}
        session['guesses'] = {test_id: ["mango", "mango", "mango", "mango", "mango", "mango"]}
        session['feedbacks'] = {test_id: ["RYRRR", "RYRRR", "RYRRR", "RYRRR", "RYRRR", "RYRRR"]}
    response = client.post(url, json=data, headers=headers)
    print(response.get_json())
    assert(response.status_code == 422)