import pytest
from flask import Flask
from typing import Generator
from wordle import create_app
from flask.testing import FlaskClient
from flask.testing import FlaskCliRunner

@pytest.fixture
def app() -> Generator[Flask, None, None]:
    app = create_app({'TESTING': True})
    yield app

@pytest.fixture
def client(app: Flask) -> FlaskClient:
    return app.test_client()

@pytest.fixture
def runner(app: Flask) -> FlaskCliRunner:
    return app.test_cli_runner()