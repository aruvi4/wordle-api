# Building a Wordle API with TDD, flask, and good ol' Unix-fu.

## Setting up the contract

Let's start our journey with its core piece of functionality, the _judge_ function that evaluates a guess against a correct answer and returns wordle-style feedback.

There is a 'new' way of doing things called 'Test Driven Development'. I say new, but it's one of those rigorous ways of doing things that was conceptualized thirty years ago that everyone hopes to one day be worthy of. So we shall dip our feet into its waters and see for ourselves.

One thing that TDD enforces is that you think seriously about the contract that your function represents, and what its expected inputs and (un)usual outputs look like. What should our judge function look like? Let's make a `wordle` directory and create a `game.py` file within it. It should contain the contract of the method, and the method should return a dummy output like so:

```
def judge(guess: str, answer: str) -> str:
	return "?????"
```

## Writing the tests

TDD dictates that we now write failing test cases for the features we'd like to add. This is where I code-as-document the expected behaviour of the program, and assert my expected outputs against what the function actually does. I return to the parent directory and make a `tests` directory, and create `test_judge.py` within it. Below is one of the tests I've written:

```
from wordle.game import judge

def test_judge_allgreens():
    answer = 'SPEAR'
    guess = 'SPEAR'
    expected_feedback = 'GGGGG'
    assert(judge(guess, answer) == expected_feedback)

```

## Executing the tests

We'll be using the `pytest` module to help us run our tests in an organized, measurable way. If you're not in a virtual environment already, the moment your project has frameworks it depends on is a good time to create and use a virtual environment. The `venv` lets you isolate your project's requirements within an environment with its own binaries and libraries, meaning you can exert fine-grained control over the versions of python and external libraries that your project depends on. It also has the benefit of not having to mess with your global python installation.

If you don't have `pip3` yet, get `pip3` via your preferred way of installing applications. If you don't have `venv`, install it with `pip3`. Once everything's set up, execute `$python3 -m venv .venv` to create a `.venv` directory in the current directory and create a portable python inside it. It should quietly succeed.

Now activate the `venv` by running the script provided like so: `$source .venv/bin/activate`. Most shell setups will now indicate in some way that this is the active virtual environment your commands will be executed in. In my bash shell, it looks something like `(.venv) aruvi@aruvi: ~/wordle_app $`

We're all set now to do `$pip3 install pytest` and it should get us ready to run our tests. Now make sure you're in the parent directory and when you `ls` you see both `wordle` and `tests` directories in the output. Now execute `python3 -m pytest` and ensure that pytest recognises and runs our test. Our test should, of course, miserably fail, and pytest will tell us we're in the red, that the expected output was `"GGGGG"` but our function returned `"?????"`.

Write the rest of the tests for every scenario you can imagine, especially edge cases. That's the first step of TDD. My full test script is over at [gitlab.](https://gitlab.com/aruvi4/wordle-api/-/blob/main/tests/test_judge.py?ref_type=heads)

## Bare minimum implementation

The second step of TDD is to write a bare-minimum implementation of the `judge` function which now must fulfil its contract and pass its test cases.

Here's what I wrote:

```
RED, GREEN, YELLOW = 'R', 'G', 'Y'

def judge(guess: str, answer: str):
    feedback = ""
    guess, answer = guess.lower(), answer.lower()
    for g_ch, a_ch in zip(guess, answer):
        if g_ch not in answer:
            feedback += RED
        elif g_ch == a_ch:
            feedback += GREEN
        else:
            feedback += YELLOW
    return feedback
```

Once you're done, run pytest again and you'll see that lovely all-green output that you'll learn to yearn for.